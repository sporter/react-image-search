import React, { Component } from 'react';
import PropTypes from 'prop-types';
import VideoItem from "./VideoItem";
require("../styles/MainApp.css");
require("../styles/Results.css");

class VideoResults extends Component {
    render() {
        let videoListContent;
        const { videos } = this.props;

        // displays overlay and appends video element to it on click
        const on = (e, vid)=>{
            e.preventDefault();
            document.getElementById("overlay").style.display = "block";
            const video = document.createElement("video");
            video.setAttribute("id", "previewVideo");
            video.setAttribute("controls", "true");
            video.setAttribute("class", "center");
            video.setAttribute("src", vid.videos.medium.url);
            document.getElementById("overlay").appendChild(video);
        };

        if (videos) {
            videoListContent = (
                <div className={"flex-container"}>
                    {videos.map(vid => (
                        <VideoItem vid={vid} handler={(e) => on(e, vid)}/>
                    ))}
                </div>
            );
        } else {
            videoListContent = null;
        }

        return (
            <div>
                {videoListContent}
            </div>
        );
    }
}

VideoResults.propTypes = {
    videos: PropTypes.array.isRequired
};

export default VideoResults;