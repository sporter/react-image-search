import React, { Component } from 'react';
require("../styles/Results.css");

class VideoItem extends Component {
    render() {
        const {vid} = this.props;

        const getFileTypeIcon =(video) => {
            const urlParts = video.pageURL.split('/');
            return (urlParts[3] === "videos") ? "/videri-project/build/images/video@2x.png" : "videri-project/build/images/image@2x.png" ;
        };

        const getResolution =(video) => {
            return video.videos.medium.width + ' x ' + video.videos.medium.height;
        };
        
        const getDuraton = (seconds) => {
                seconds= Number(seconds);
                const h = Math.floor(seconds/ 3600);
                const m = Math.floor(seconds% 3600 / 60);
                const s = Math.floor(seconds% 3600 % 60);

                const hDisplay = h > 0 ? h + (h === 1 ? " hour " : " hours, ") : "";
                const mDisplay = m > 0 ? m + (m === 1 ? " minute " : " minutes ") : "";
                const sDisplay = s > 0 ? s + (s === 1 ? " second" : " seconds") : "";
                return hDisplay + mDisplay + sDisplay;
            };

        const getCreationDate =(video) => {
            const urlParts = video.userImageURL.split('/');
            let creationDate = '';
            for (let i = 4; i < 7; i++) {
                creationDate += urlParts[i];
                if(i !== 6) {
                    creationDate += "/";
                }
            }

            return creationDate;
        };

        const getFileName =(video) => { return video.tags.split(',')[0] + " " + video.type + " hd.mp4"};

        const imageTile = {
            margin: '5px',
            background: 'white'
        };

        return (
            <div
                onClick={this.props.handler}
                title={vid.tags}
                key={vid.id}
                style={imageTile}
            >
                <div className="iconStyle">
                    <img src={ 'https://i.vimeocdn.com/video/' + vid.picture_id + "_100x75.jpg"} alt='' />
                </div>
                <div className="descriptionStyle">
                    <div>{getFileName(vid)}</div>
                    <div><img className="fileTypeIcon" src={window.location.origin + getFileTypeIcon(vid)} alt=""/><span>{getDuraton(vid.duration)}</span></div>
                    <div>{getResolution(vid)}</div>
                    <div>Created</div>
                    <div>{getCreationDate(vid)}</div>
                </div>
            </div>
        );
    }
}

export default VideoItem;


