import React, { Component } from 'react';
import MainApp from './MainApp';
import '../styles/Login.css';

// const validateEmail = (email) => {
//     const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     return re.test(String(email).toLowerCase());
// };
//
// const validatePassword = (password) => {
//     const re = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
//     return re.test(String(password));
// };

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { username: '', password: ''};

        this.usernameChangeHandler = this.usernameChangeHandler.bind(this);
        // this.passwordChangeHandler = this.passwordChangeHandler.bind(this);
        this.loginHandler = this.loginHandler.bind(this);
    }

    // handles active changes in username field
    usernameChangeHandler(event) {
        this.setState({ username: event.target.value });
    }

    // handles active changes in password field
    // passwordChangeHandler(event) {
    //     this.setState({ password: event.target.value });
    // }


    // Validates username and password meet desired criteria when user attempts to submit
    loginHandler(event) {
        event.preventDefault();
        // if (this.state.username && this.state.password) {
        //     validateEmail(this.state.username) && validatePassword(this.state.password) ?
        //         (this.setState({submitted: true, username: this.state.username, password: this.state.password}),
        //         (document.body.style.background = "none"),
        //         (document.body.style.backgroundColor = "#ededed"))
        //         :
        //         alert('Password must contain at least 8 characters, an uppercase letter, a lowercase letter, and a special character. Please try Again');
        // }

        if (this.state.username) {
            this.setState({submitted: true, username: this.state.username});
            document.body.style.background = "none";
            document.body.style.backgroundColor = "#ededed";
        }

    }

    // places cursor inside input field upon loading app
    componentDidMount() {
        this.loginInput.focus();
    }

    render() {
        if (this.state.submitted) {
            // if the Login form is submitted show the main part of the app
            return (<MainApp username={this.state.username} />);
        }

        return (
            // Show a Login form if one has not been submitted yet

            <form onSubmit={this.loginHandler} >
                <div className="username-container">
                    <div className="signIn">SIGN IN</div>
                    <div>
                        <input
                            className="login"
                            type="text"
                            onChange={this.usernameChangeHandler}
                            placeholder="Enter your name"
                            required
                            ref={(input)=>{this.loginInput = input; }}
                        />
                    </div>
                    <input type="submit" value="SIGN IN"/>
                </div>
            </form>
        );
    }
}

export default Login;
